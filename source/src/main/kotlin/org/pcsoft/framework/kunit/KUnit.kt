package org.pcsoft.framework.kunit

import java.math.BigDecimal

/**
 * Basic interface for all units. Can be used for custom extensions. This type is immutable.
 *
 * This basic class contains all relevant information about the unit. These are:
 * * **Value** - The raw value of this unit based value as BigDecimal.
 * * **Exponent** - The raw exponent for this unit as byte. Normally it is one.
 * If it is negative the part of this unit is in denominator.
 *
 * Please note that this class can use as extension for custom units not included in this framework.
 */
abstract class KUnit<U : KUnitType> protected constructor(
    val rawValue: BigDecimal = BigDecimal.valueOf(0),
    val rawExponent: Byte = 1
) {
    abstract fun toValue(type: U): BigDecimal
    abstract fun toValue(prefix: KUnitPrefix, type: U): BigDecimal

    abstract override fun toString(): String
    abstract fun toString(type: U): String
    abstract fun toString(prefix: KUnitPrefix, type: U): String

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is KUnit<*>) return false

        if (rawValue != other.rawValue) return false
        if (rawExponent != other.rawExponent) return false

        return true
    }

    override fun hashCode(): Int {
        var result = rawValue.hashCode()
        result = 31 * result + rawExponent
        return result
    }
}