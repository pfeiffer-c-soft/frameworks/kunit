package org.pcsoft.framework.kunit

import java.util.Locale

/**
 * Represent an enumeration with all unit based country types. It is used to differ between specific
 * country units like meters vs. miles etc.
 */
enum class KUnitCountry(private vararg val locales: Locale) {
    Europe(Locale.forLanguageTag("de"), Locale.forLanguageTag("fr")),
    UnitedKingdom(Locale.UK, Locale.ENGLISH),
    America(Locale.CANADA, Locale.US),
    Other,
    ;

    companion object {
        val default: KUnitCountry
            get() {
                for (value in KUnitCountry.entries) {
                    if (value.locales.contains(Locale.getDefault()))
                        return value
                }

                return Other
            }
    }
}