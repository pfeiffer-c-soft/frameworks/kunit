@file:Suppress("unused")

package org.pcsoft.framework.kunit

/**
 * Represent an enumeration with all known prefixes vor value units.
 */
enum class KUnitPrefix(
    val displayName: String,
    val displayAbbreviation: String,
    val factor: Double,
) {
    Atto("Atto", "a", 0.00_000_000_000_000_000_1),
    Femto("Femto", "f", 0.00_000_000_000_000_1),
    Pico("Pico", "p", 0.00_000_000_000_1),
    Nano("Nano", "n", 0.00_000_000_1),
    Micro("Micro", "\u00B5", 0.00_000_1),
    Milli("Milli", "m", 0.001),
    Centi("Centi", "c", 0.01),
    Deci("Deci", "d", 0.1),

    Default("", "", 1.0),

    Deca("Deca", "da", 10.0),
    Hecto("Hecto", "h", 100.0),
    Kilo("Kilo", "k", 1000.0),
    Mega("Mega", "M", 1_000_000.0),
    Giga("Giga", "G", 1_000_000_000.0),
    Tera("Tera", "T", 1_000_000_000_000.0),
    Peta("Peta", "P", 1_000_000_000_000_000.0),
    Exa("Exa", "E", 1_000_000_000_000_000_000.0),
}