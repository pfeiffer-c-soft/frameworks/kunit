package org.pcsoft.framework.kunit

/**
 * Interface to define an enumeration for a typical unit type.
 *
 * It contains information about defaults depends on current language, display name and abbreviation.
 */
interface KUnitType {
    val displayName: String
    val displayAbbreviation: String
    val factor: Double
    val countryDefault: Array<KUnitCountry>
}