@file:Suppress("unused")

package org.pcsoft.framework.kunit.length

import org.pcsoft.framework.kunit.KUnit
import org.pcsoft.framework.kunit.KUnitPrefix
import org.pcsoft.framework.kunit.utils.times
import java.math.BigDecimal

class KLengthUnit internal constructor(rawValue: BigDecimal) : KUnit<KLengthUnitType>(rawValue, 1) {
    override fun toValue(type: KLengthUnitType): BigDecimal = rawValue * type.factor

    override fun toValue(prefix: KUnitPrefix, type: KLengthUnitType): BigDecimal =
        rawValue * type.factor * prefix.factor

    override fun toString(): String = toString(KLengthUnitType.default)

    override fun toString(type: KLengthUnitType): String =
        "${rawValue * type.factor} ${type.displayAbbreviation}"

    override fun toString(prefix: KUnitPrefix, type: KLengthUnitType): String =
        "${rawValue * type.factor * prefix.factor} ${prefix.displayAbbreviation}${type.displayAbbreviation}"
}

infix fun Number.`as`(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor))

infix fun Number.atto(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Atto.factor))
infix fun Number.femto(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Femto.factor))
infix fun Number.pico(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Pico.factor))
infix fun Number.nano(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Nano.factor))
infix fun Number.micro(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Micro.factor))
infix fun Number.milli(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Milli.factor))
infix fun Number.centi(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Centi.factor))
infix fun Number.deci(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Deci.factor))

infix fun Number.deca(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Deca.factor))
infix fun Number.hecto(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Hecto.factor))
infix fun Number.kilo(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Kilo.factor))
infix fun Number.mega(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Mega.factor))
infix fun Number.giga(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Giga.factor))
infix fun Number.tera(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Tera.factor))
infix fun Number.peta(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Peta.factor))
infix fun Number.exa(type: KLengthUnitType): KLengthUnit =
    KLengthUnit(BigDecimal.valueOf(this.toDouble() * type.factor * KUnitPrefix.Exa.factor))
