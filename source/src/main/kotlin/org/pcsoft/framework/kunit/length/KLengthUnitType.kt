package org.pcsoft.framework.kunit.length

import org.pcsoft.framework.kunit.KUnitCountry
import org.pcsoft.framework.kunit.KUnitType
import java.util.Locale

enum class KLengthUnitType(
    override val displayName: String,
    override val displayAbbreviation: String,
    override val factor: Double,
    vararg countryDefault: KUnitCountry
) : KUnitType {
    Meter("meter", "m", 1.0, KUnitCountry.Europe, KUnitCountry.Other),
    Food("food", "f", 0.3048),
    Inch("inch", "in", 0.0254, KUnitCountry.America, KUnitCountry.UnitedKingdom),
    Yard("yard", "ya", 0.9144),
    Mile("mile", "mi", 1609.34)
    ;

    companion object {
        val default: KLengthUnitType
            get() {
                val defCountry = KUnitCountry.default
                for (value in entries) {
                    if (value.countryDefault.contains(defCountry))
                        return value
                }

                throw IllegalStateException(
                    "Unable to find default ${KLengthUnitType::class.simpleName} " +
                            "for $defCountry (${Locale.getDefault()}"
                )
            }
    }

    override val countryDefault: Array<KUnitCountry> = countryDefault.toList().toTypedArray()

}