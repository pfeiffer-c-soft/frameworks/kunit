package org.pcsoft.framework.kunit.utils

import java.math.BigDecimal

internal operator fun BigDecimal.times(value: Double): BigDecimal = this * BigDecimal.valueOf(value)

internal operator fun BigDecimal.div(value: Double): BigDecimal = this / BigDecimal.valueOf(value)