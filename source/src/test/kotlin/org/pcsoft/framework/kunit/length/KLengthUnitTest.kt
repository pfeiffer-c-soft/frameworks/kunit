package org.pcsoft.framework.kunit.length

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import java.math.BigDecimal

class KLengthUnitTest {

    @Test
    fun createValue() {
        val length1 = 1000.0 `as` KLengthUnitType.Meter
        val length2 = 1.0 kilo KLengthUnitType.Meter

        Assertions.assertEquals(length1, length2)
    }

    @ParameterizedTest
    @EnumSource(KLengthUnitType::class, names = ["Meter", "Yard", "Food", "Inch", "Mile"])
    fun testWithoutPrefix(type: KLengthUnitType) {
        val length = 1 `as` type
        Assertions.assertEquals(BigDecimal.valueOf(type.factor), length.rawValue)
    }

}